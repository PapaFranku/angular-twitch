import { AngularTwitchPage } from './app.po';

describe('angular-twitch App', function() {
  let page: AngularTwitchPage;

  beforeEach(() => {
    page = new AngularTwitchPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
