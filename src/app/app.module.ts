import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { SearchComponent } from './components/search/search.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { routes } from './app.routes';
import { TwitchService } from './services/twitch.service';
import { ChannelComponent } from './components/channel/channel.component';
import { SafeUrlPipe } from './pipes/safe-url.pipe';

@NgModule({
  declarations: [ AppComponent, SearchComponent, NavbarComponent, ChannelComponent, SafeUrlPipe ],
  imports: [ BrowserModule, FormsModule, HttpModule, routes ],
  providers: [ TwitchService ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
