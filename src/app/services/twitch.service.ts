import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()

export class TwitchService 
{
  private authHeader: Headers;

  constructor(private http: Http) 
  {
    this.authHeader = new Headers();
    var token: string = "PLACE Client-Id HERE";
    this.authHeader.append("Client-ID", token);
  }

  searchChannels(searchString: string)
  {
    var url = "https://api.twitch.tv/kraken/search/channels?query=" + searchString;

    return this.http.get(url, { headers: this.authHeader }).map(res => res.json());
  }

  getChannel(name: string)
  {
    var url = "https://api.twitch.tv/kraken/channels/" + name;

    return this.http.get(url, { headers: this.authHeader }).map(res => res.json());
  }

  getStream(id: string)
  {
    var url = "https://api.twitch.tv/kraken/streams/" + id;

    return this.http.get(url, { headers: this.authHeader }).map(res => res.json());
  }
}
