import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { SearchComponent } from './components/search/search.component';
import { ChannelComponent } from './components/channel/channel.component';


const routing: Routes =
[
    {
        path: "",
        component: SearchComponent
    },
    {
        path: "channel/:name",
        component: ChannelComponent
    }
];

export const routes: ModuleWithProviders = RouterModule.forRoot(routing);