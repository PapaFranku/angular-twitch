import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { TwitchService } from '../../services/twitch.service';
import { SafeUrlPipe } from '../../pipes/safe-url.pipe';

@Component({
  moduleId: module.id,
  selector: 'channel',
  templateUrl: './channel.component.html',
  styleUrls: ['./channel.component.css']
})

export class ChannelComponent implements OnInit 
{
  channelObject: any;
  streamUrl: string;

  constructor(private twitchService: TwitchService, private route: ActivatedRoute) 
  { 

  }

  ngOnInit() 
  {
    this.route.params.map(params => params['name'])
      .subscribe((id) => { this.twitchService.getChannel(id)
        .subscribe(channel => { this.channelObject = channel; console.log(channel); 
          this.streamUrl = "http://player.twitch.tv/?channel=" + this.channelObject.name + "&autoplay=false"; }) });
  }
}
