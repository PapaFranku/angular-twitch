import { Component } from '@angular/core';
import { TwitchService } from '../../services/twitch.service';

@Component({
  moduleId: module.id,
  selector: 'search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})

export class SearchComponent 
{
  searchResults: any;

  constructor(private twitchService: TwitchService) { }

  searchChannels(searchString: string)
  {
    this.twitchService.searchChannels(searchString)
      .subscribe(res => { this.searchResults = res.channels; console.log(res); });
  }
}
